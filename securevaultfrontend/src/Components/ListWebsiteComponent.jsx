import React, { Component } from 'react';
import WebsiteService from '../Services/WebsiteService';


class ListWebsiteComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            websites : []
        }
        this.addWebsite = this.addWebsite.bind(this);
        this.editWebsite = this.editWebsite.bind(this);
        this.deleteWebsite = this.deleteWebsite.bind(this);
    }

    componentDidMount() {
        WebsiteService.getWebsites().then((res) => {
            this.setState({websites: res.data});
        });
    }

    addWebsite(){
        this.props.history.push('/addWebsite');
    }

    editWebsite(id) {
        this.props.history.push('/updateWebsite/'+ id);
    }

    deleteWebsite(id) {
        WebsiteService.deleteWebsite(id).then( res => {
            this.setState({websites : this.state.websites.filter(website => website.id !== id) });
        });
    }

    

    render() {
        return (
            <div>
                <h2 className="text-center">Websites List</h2>
                <div className="row">
                    <table className = "table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Website</th>
                                <th>Username</th>
                                <th>Password</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.websites.map(
                                    website => 
                                    <tr key={website.id}>
                                        <td>{website.name}</td>
                                        <td>{website.username}</td>
                                        <td>{website.password}</td>
                                        <button className="btn btn-info" style={{marginLeft: "10px"}} onClick={ () => this.editWebsite(website.id)}>Update</button>
                                        <button className="btn btn-info" style={{marginLeft: "10px"}}  onClick={ () => this.deleteWebsite(website.id)}>Delete</button>
                                    </tr>
                                )
                            }

                        </tbody>
                    </table>
                </div>   
                <div className= "row">
                    <button className="btn btn-primary" onClick={this.addWebsite}> + </button>
                </div>          
            </div>
        );
    }
}

export default ListWebsiteComponent;