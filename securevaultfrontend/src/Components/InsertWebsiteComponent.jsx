import React, { Component } from 'react';
import WebsiteService from '../Services/WebsiteService';

class InsertWebsiteComponent extends Component {

    constructor(props) {
        super(props) 

        this.state = {
            Name: '',
            Username: '',
            Password: ''
        }
        
        this.changeNameHandler=this.changeNameHandler.bind(this);
        this.changeUsernameHandler=this.changeUsernameHandler.bind(this);
        this.changePasswordHandler=this.changePasswordHandler.bind(this);
        this.saveWebsite = this.saveWebsite.bind(this);
        this.cancel=this.cancel.bind(this);

    }

    saveWebsite = (e) => {
        e.preventDefault();
        let website = {name: this.state.Name, username: this.state.Username, password: this.state.Password};
        console.log('website => ' + JSON.stringify(website));
        WebsiteService.createWebsite(website).then(res => {
            this.props.history.push('/getWebsites');
        });
    }

    changeNameHandler= (event) => {
        this.setState({Name: event.target.value});
    }

    changeUsernameHandler= (event) => {
        this.setState({Username: event.target.value});
    }

    changePasswordHandler= (event) => {
        this.setState({Password: event.target.value});
    }

    cancel() {
        this.props.history.push('/getWebsites');
    }



    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Website</h3>
                            <div className="card-body">
                                <form>
                                    <div 
                                        className="form-group">
                                        <label>Website Name: </label>
                                        <input placeholder="Name" name="name" className="form-control"
                                        value={this.state.Name} onChange={this.changeNameHandler} />
                                    </div>

                                    <div 
                                        className="form-group">
                                        <label>Username: </label>
                                        <input placeholder="Username" name="Username" className="form-control"
                                        value={this.state.Username} onChange={this.changeUsernameHandler} />
                                    </div>

                                    <div 
                                        className="form-group">
                                        <label>Password: </label>
                                        <input placeholder="Password" name="Password" className="form-control"
                                        value={this.state.Password} onChange={this.changePasswordHandler} />
                                    </div>

                                    <button className="btn btn-success" onClick={this.saveWebsite}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel} style={{marginLeft: "10px"}}>Cancel</button>


                                </form>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default InsertWebsiteComponent;