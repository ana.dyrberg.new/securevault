import './App.css';
import ListWebsiteComponent from './Components/ListWebsiteComponent'
import HeaderComponent from './Components/HeaderComponent';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import InsertWebsiteComponent from './Components/InsertWebsiteComponent';
import UpdateWebsiteComponent from './Components/UpdateWebsiteComponent';

function App() {
  return (
      <div>
        <Router> 
          <div className="container">
            <HeaderComponent />
            <div className="container">
              <Switch>
                <Route path="/getWebsites" component={ListWebsiteComponent}></Route>
                <Route path="/addWebsite" component={InsertWebsiteComponent }></Route>
                <Route path="/updateWebsite/:id" component={UpdateWebsiteComponent }></Route>
              </Switch>
            </div>
        </div>
        </Router>
      </div>
  );
}

export default App;
