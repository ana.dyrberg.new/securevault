import axios from 'axios'
const WEBSITE_API_BASE_URL = "http://localhost:8080/api/v1/";


class WesiteService {
    getWebsites() {
        return axios.get(WEBSITE_API_BASE_URL+"getWebsites");
    }

    createWebsite(website){
        return axios.post(WEBSITE_API_BASE_URL+"addWebsites", website);
    }

    getWebsiteById(websiteId){
        return  axios.get(WEBSITE_API_BASE_URL+"website/"+ websiteId);
    }

    updateWebsite(website, websiteId){
        return axios.put(WEBSITE_API_BASE_URL+"editWebsite/"+ websiteId, website);
    }

    deleteWebsite(websiteId){
        return axios.delete(WEBSITE_API_BASE_URL+"deleteWebsite/"+websiteId);
    }

}

export default new WesiteService()