# Building Systems for People - SecureVault

### Front-End<br />
Main Page:                         http://localhost:3000/getWebsites <br />

### Back-End API Endpoints<br />
GET All Websites Request:          http://localhost:8080/api/v1/getWebsites<br />
GET A Website Request:             http://localhost:8080/api/v1/Website/<website id><br />
POST Website Request:              http://localhost:8080/api/v1/addWebsites
PUT Website Request:               http://localhost:8080/api/v1/editWebsite/<website id><br />
DELETE Website Request:            http://localhost:8080/api/v1/deleteWebsite/<website id><br />


### Prometheus<br />
Main Page:                         http://localhost:9090/<br />
Targets:                           http://localhost:9090/targets<br />
Recording Rules:                   http://localhost:9090/rules<br />
Recoring Rule - CPU Usage :        http://localhost:9090/graph?g0.expr=cpu_usage&g0.tab=1&g0.stacked=0&g0.show_exemplars=0.g0.range_input=1h.<br />

### NGINX Load Balancer<br />
Backend Load Balancer:             http://localhost:8081<br />