package com.secureVautlBackEnd.secureVaultBackEnd.repository;
import com.secureVautlBackEnd.secureVaultBackEnd.model.website;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface websitesRepository extends JpaRepository<website, Long> {



}