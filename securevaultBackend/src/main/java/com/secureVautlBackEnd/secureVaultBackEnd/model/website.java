package com.secureVautlBackEnd.secureVaultBackEnd.model;
import javax.persistence.*;

@Entity
@Table(name= "password_storage_system")
public class website {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "site_name")
    private String name;

    @Column(name = "url")
    private String link;

    @Column(name="username")
    private String username;

    @Column(name="password")
    private String password;

    public website(String name, String link, String username, String password) {
        this.name = name;
        this.link = link;
        this.username = username;
        this.password = password;
    }

    public website() {

    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
