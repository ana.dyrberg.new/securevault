package com.secureVautlBackEnd.secureVaultBackEnd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecureVaultBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecureVaultBackEndApplication.class, args);
	}

}
