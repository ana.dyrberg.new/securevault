package com.secureVautlBackEnd.secureVaultBackEnd.controller;
import com.secureVautlBackEnd.secureVaultBackEnd.exception.ResourceNotFoundException;
import com.secureVautlBackEnd.secureVaultBackEnd.model.website;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.secureVautlBackEnd.secureVaultBackEnd.repository.websitesRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:3000")  //this is important otherwise the api will not be accessible
@RestController
@RequestMapping("/api/v1/")
public class websiteController {

    @Autowired
    private websitesRepository websitesRespository;

    // develop get all websites api call
    // when /api/v1/webistes is called on browser, this method will be executed
    @GetMapping("/getWebsites")
    public List<website> getAllWebsites(){
        return websitesRespository.findAll();
    }

    //add websites REST API
    @PostMapping("/addWebsites")
    public website insertWebsite(@RequestBody website web) {
        return websitesRespository.save(web);
    }


    // get employee by ID rest API
    @GetMapping("/website/{id}")
    public ResponseEntity<website> getWebsiteById(@PathVariable Long id){
        website web = websitesRespository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Website does not exist with ID : "+id));
        // returns a 200 ok an the web object in the body
        return ResponseEntity.ok(web);
    }

    // update website rest API
    @PutMapping("/editWebsite/{id}")
    public ResponseEntity<website> updateWebsiteById(@PathVariable Long id,@RequestBody website websiteDetails) {
        website Website = websitesRespository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Website does not exist with ID : "+id));
        Website.setName(websiteDetails.getName());
        Website.setLink(websiteDetails.getLink());
        Website.setUsername(websiteDetails.getUsername());
        Website.setPassword(websiteDetails.getPassword());
        website updatedWebsiteInfo = websitesRespository.save(Website);
        return ResponseEntity.ok(updatedWebsiteInfo);
    }

    //delete employee Rest API
    @DeleteMapping("/deleteWebsite/{id}")
    public ResponseEntity<Map<String,Boolean>> deleteWebsite(@PathVariable Long id) {
        website Website = websitesRespository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Website does not exist with ID : "+id));
        websitesRespository.delete(Website);
        Map<String, Boolean> resp = new HashMap<>();
        resp.put("Deleted", Boolean.TRUE);
        return ResponseEntity.ok(resp);
    }

}
